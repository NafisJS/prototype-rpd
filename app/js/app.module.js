(function() {
  'use strict';

  angular
    .module('rpdApp',[
        'ui.materialize',
        'ui.router',
        'ngResource',
        'xeditable'
    ]);

})();
