(function() {
	angular
		.module('rpdApp')
		.controller('LoginController', LoginController);

	function LoginController($scope, loginService, $state) {
		console.log('LoginController');
		var vm = this;
		vm.user = null;   

	// Функция авторизации пользователя
		vm.loginSubmit = function (userForm, loginForm){

			loginService.getData().then(function(msg) { 
			vm.user=msg.data.user; 
				if(loginForm.$valid){
					if (vm.user.username == userForm.username && vm.user.password == userForm.password){
						$state.go('main');
					}else {
						Materialize.toast('Логин или пароль не верен! Повторите ввод.', 10000);
					}
				}
			});
		}
	// Конец Функции авторизации

	// Функция отмены авторизации
		vm.cancelLogin = function () {
			$state.go('home');
		}
	// Конец Функции авторизации
	}
})();
