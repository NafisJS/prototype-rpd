(function() {
	angular
	.module('rpdApp')
	.controller('MainController', MainController);

	function MainController($scope, $http){
		$scope.getSoftCategory = {};

		$scope.getCategory = function(){
			$http.get('api/catsoft')
			.then(function(data){
				$scope.getSoftCategory = data.data.data;
			},
			function(err){
				console.log(err);
			});
		};

		$scope.getCategory();

		$scope.saveCategory = function(data, id) {
			angular.extend(data, {id: id});
			$http.put('api/catsoft/' + id + '?name=' + data.name + '&comment=' + data.comment);
		};

		$scope.addCategory = function(){
			$scope.inserted = {
				name: '',
				comment: ''
			};
			$http.post('api/catsoft' + '?name=' + $scope.inserted.name + '&comment=' + $scope.inserted.comment)
			.then(function(data){
				$scope.inserted.id = parseInt(data.data.data[0].currval);
				$scope.getSoftCategory.push($scope.inserted);
			},function(err){
				console.log(err);
			});
		};

		$scope.removeCategory = function(index, id){
			$scope.getSoftCategory.splice(index, 1);
			$http.delete('api/catsoft/' + id);
		};

		$scope.rpdData = {
			Department: "ЭТФ (Электротехнический факультет)",
			Chair:      "ИТАС (Информационные технологии и автоматизированные системы)",
			Name:       "Проектирование информационных систем",
			EduProgram: "академической магистратуры",
			Direction:  '09.04.04 "Программная инженерия"',
			Profile:    "Разработка программно-информационных систем",
			Skill:      "магистр",
			EduForm:    "очная",
			Course:     "2",
			Semester:   "3",
			Credit:     "4",
			Hour:       "144",
			EkzSem:     "1 семестр",
			ZachSem:    "1 семестр",
			ProjSem:    "1 семестр",
			WorkSem:    "1 семестр",
			Year:       "2016"
		}
	}
})();
