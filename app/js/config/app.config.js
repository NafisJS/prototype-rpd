angular
    .module("rpdApp")
    .config(appConfig);

appConfig.$inject =['$stateProvider', '$urlRouterProvider', '$qProvider'];

function appConfig($stateProvider, $urlRouterProvider, $qProvider) {
    $qProvider.errorOnUnhandledRejections(false);

    $urlRouterProvider.otherwise("/main");

    $stateProvider
        .state('home', {
            url: '/home',
            templateUrl: "views/home.html",
            controller: "HomeController"
        })
        .state('login', {
            url: '/login',
            templateUrl: "views/login.html",
            controller: "LoginController"
        })
        .state('logout', {
            url: '/home'
        })
        .state('main', {
            url: '/main',
            templateUrl: "views/main.html",
            controller: "MainController"
        })
        .state('search', {
            parent: 'main',
            url: '/search',
            templateUrl: "views/search.html",
            controller: "MainController"
        })
        .state('directory', {
            parent: 'main',
            url: '/directory',
            templateUrl: "views/directory.html",
            controller: "MainController"
        })
        .state('catsoft', {
            parent: 'directory',
            url: '/catsoft',
            templateUrl: "views/catsoft.html",
            controller: "MainController"
        })
        .state('discipline', {
            parent: 'main',
            url: '/discipline',
            templateUrl: "views/discipline.html",
            controller: "MainController"
        })
        .state('develop', {
            parent: 'main',
            url: '/develop',
            templateUrl: "views/develop.html",
            controller: "MainController"
        })
        .state('page1', {
            parent: 'develop',
            url: '/page1',
            templateUrl: "views/page1.html",
            controller: "MainController"
        })
        .state('page2', {
            parent: 'develop',
            url: '/page2',
            templateUrl: "views/page2.html",
            controller: "MainController"
        })
        .state('page3', {
            parent: 'develop',
            url: '/page3',
            templateUrl: "views/page3.html",
            controller: "MainController"
        })
        .state('page4', {
            parent: 'develop',
            url: '/page4',
            templateUrl: "views/page4.html",
            controller: "MainController"
        })
        .state('page5', {
            parent: 'develop',
            url: '/page5',
            templateUrl: "views/page5.html",
            controller: "MainController"
        })
        .state('page6', {
            parent: 'develop',
            url: '/page6',
            templateUrl: "views/page6.html",
            controller: "MainController"
        })
        .state('page7', {
            parent: 'develop',
            url: '/page7',
            templateUrl: "views/page7.html",
            controller: "MainController"
        })
        .state('page8', {
            parent: 'develop',
            url: '/page8',
            templateUrl: "views/page8.html",
            controller: "MainController"
        })
        .state('page9', {
            parent: 'develop',
            url: '/page9',
            templateUrl: "views/page9.html",
            controller: "MainController"
        });
}
