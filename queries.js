var promise = require('bluebird');

var options = {
    promiseLib: promise
};

var pgp = require('pg-promise')(options);

var connectionString = 'postgres://postgres:postgres@localhost:5432/arm-rpd';

var db = pgp(connectionString);

function getAllCatSoft(req, res, next){
    db.query('Select * from categorysoftware order by 1')
        .then(function(data){
            res.status(200)
                .json({
                    status: 'success',
                    data: data,
                    message: 'Get all catsoft'
                });
        })
        .catch(function (err){
            return next(err);
        });
}

function getSingleCatSoft(req, res, next){
    var catSoftID = parseInt(req.params.id);
    db.query('Select * from categorysoftware where id = $1', catSoftID)
        .then(function(){
            res.status(200)
                .json({
                    status: 'success',
                    message: 'Get ONE categorysoftware'
                });
        })
        .catch(function (err) {
            return next(err);
        });
}

function createCatSoft(req, res, next) {
    db.query('insert into categorysoftware(name, comment)' +
            'values(${name},${comment});'+
            "SELECT currval(pg_get_serial_sequence('categorysoftware', 'id'));", req.query)
        .then(function (data){
                res.status(200)
                    .json({
                        status: 'success',
                        data: data,
                        message: 'Inserted one categorysoftware'
                    });
        })
        .catch(function(err){
            return next(err);
        });

}

function updateCatSoft(req, res, next) {
    db.query('update categorysoftware set name = $1, comment = $2 where id = $3; Commit;',
                [req.query.name, req.query.comment, parseInt(req.params.id)])
        .then(function(){
            res.status(200)
                .json({
                    status: 'success',
                    message: 'Updated one categorysoftware'
                });
        })
        .catch(function(err){
            return next(err);
        });
}

function removeCatSoft(req, res, next){
    var catSoftID = parseInt(req.params.id);
    db.result('delete from categorysoftware where id = $1', catSoftID)
        .then(function (result){
            res.status(200)
                .json({
                    status: 'success',
                    message: `Removed ${result.rowCount} categorysoftware`
                });
        })
        .catch(function(err){
            return next(err);
        });
}
// END Categorysoftware
module.exports = {
    getAllCatSoft          : getAllCatSoft,
    getSingleCatSoft       : getSingleCatSoft,
    createCatSoft          : createCatSoft,
    updateCatSoft          : updateCatSoft,
    removeCatSoft          : removeCatSoft
};
