var express = require('express');
var bodyParser  = require('body-parser');
var fs = require('fs');
var jsonfile = require('jsonfile');
var Docxtemplater = require('docxtemplater');
var JSZip = require('jszip');

var path = require('path');
var mime = require('mime');
var request = require('request');

var dataJSON = jsonfile.readFileSync(__dirname + '/page1data.json');
var content = fs.readFileSync(__dirname + '/shablon.docx','binary');
var zip = new JSZip(content);
var doc=new Docxtemplater().loadZip(zip);

var db = require('./queries');

var app = express();
app.use(express.static('app'));
app.use(bodyParser.urlencoded({extended: true}));
app.use(bodyParser.json());

var router = express.Router();
router.get('/', function(req, res){
    res.json({message: 'Welcome to our api!'});
});

router.get('/catsoft', db.getAllCatSoft);
router.get('/catsoft/:id', db.getSingleCatSoft);
router.post('/catsoft', db.createCatSoft);
router.put('/catsoft/:id', db.updateCatSoft);
router.delete('/catsoft/:id', db.removeCatSoft);

app.use('/api', router);

/*app.get('/download', function (req, res) {
	console.log('This test in server js');

	doc.setData({
		'department':	dataJSON.Department,
		'chair':		dataJSON.Chair,
		'name_rpd':		dataJSON.Name,
		'edu_program':	dataJSON.EduProgram,
		'direction':	dataJSON.Direction,
		'profile':		dataJSON.Profile,
		'skill':		dataJSON.Skill,
		'edu_form':		dataJSON.EduForm,
		'course':		dataJSON.Course,
		'semester':		dataJSON.Semester,
		'credit':		dataJSON.Credit,
		'hour':			dataJSON.Hour,
		'ekz_sem':		dataJSON.EkzSem,
		'zach_sem':		dataJSON.ZachSem,
		'proj_sem':		dataJSON.ProjSem,
		'work_sem':		dataJSON.WorkSem,
		'year': 		dataJSON.Year
	});

	doc.render();
	var buf = doc.getZip().generate({type:'nodebuffer'});
	fs.writeFileSync(__dirname+'/App/result.docx', buf);

	var file = __dirname + '/App/result.pdf';

    var data = fs.readFileSync(file);
  	res.contentType("application/pdf");

    res.send(data);

});*/

app.listen(3000, function () {
	console.log('Example app listening on port 3000! Materialize!');
});

module.exports = router;
